﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using LibGit2Sharp;

namespace portablenut.VaultConvertLib
{
	internal class GitLogic
	{
		private Repository repository;

		internal string RepositoryPath { get; private set; }
		internal portablenut.VaultConvertLib.Logging.Logger Logger { get; set; }

		internal GitLogic(string repositoryPath)
		{
			this.RepositoryPath = repositoryPath;
		}

		internal void Init()
		{
			if (!Directory.Exists(RepositoryPath))
				Directory.CreateDirectory(RepositoryPath);

			repository = Repository.Init(RepositoryPath, false, null);

			//clean dirty working directory
			repository.RemoveUntrackedFiles();
			if(repository.Commits.Count() > 0)
				repository.Reset(ResetOptions.Hard);
			
			Logger.Trace("Initialized git repository at {0}", RepositoryPath);
		}

		internal void CheckDirtyState()
		{
			Logger.Trace("Checking state of working directory");

			if (repository.Index.RetrieveStatus().Count() != 0)
			{
				var errMsg = "Dirty git working directory! This could indicate a bug";
				Logger.Warn(errMsg);
				throw new Exception(errMsg);
			}
		}

		internal void MoveFile(string source, string destination)
		{
			Logger.Jitter("Moving file {0} to {1}", source, destination);

			repository.Index.Move(source, destination);
		}

		internal void DeleteFile(string file)
		{
			Logger.Jitter("Deleting file {0}", file);

			repository.Index.Remove(file);
		}

		internal void StageChanges()
		{
			Logger.BeginTraceTime("StageChanges");
			Logger.Trace("Start staging changes");

			List<StatusEntry> changes = repository.Index.RetrieveStatus().ToList();
			List<string> modifications = changes.Where(c => c.State != FileStatus.Removed)
				.Select(c => c.FilePath).ToList();

			if (modifications.Count > 0)
				repository.Index.Stage(modifications);

			Logger.Trace("Staged {0} modifications", modifications.Count);
			Logger.EndTraceTime();
		}

		internal void CommitChanges(string changesetComment, string username, DateTime date, long version)
		{
			Logger.BeginTraceTime("CommitChanges");
			Logger.Trace("Start committing changes for version {0}", version);

			var committer = new Signature(username, username, date);
			var cmt = repository.Commit(changesetComment, committer, committer);
			var note = repository.Notes.Add(cmt.Id, version.ToString(), committer, committer, "vault");

			Logger.Trace("Done committing changes for version {0}", version);
			Logger.EndTraceTime();
		}

		internal long RetrieveLastCommittedVaultVersion()
		{
			long lastCommittedVersion = 0;
			var lastCommit = repository.Commits.FirstOrDefault();
			if (lastCommit != null)
			{
				var commitNode = lastCommit.Notes.Single(n => n.Namespace == "vault").Message;
				lastCommittedVersion = Convert.ToInt64(commitNode);
			}

			Logger.Trace("Using commit version {0} as starting point", lastCommittedVersion);
			return lastCommittedVersion;
		}
	}
}
