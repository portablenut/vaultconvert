﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace portablenut.VaultConvertLib.Logging
{
	public class LoggerEventArgs : EventArgs
	{
		public LoggerEventArgs(LogType logType, string message)
		{
			this.LogType = logType;
			this.Message = message;
		}

		public LogType LogType { get; private set; }
		public string Message { get; private set; }
	}
}
