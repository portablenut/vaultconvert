﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace portablenut.VaultConvertLib.Logging
{
	[Flags]
	public enum LogType
	{
		None = 0,
		Jitter = 1,
		Trace = 2,
		Warn = 4,
		Error = 8,

		All = Jitter | Trace | Warn | Error,
		AllButJitter = Trace | Warn | Error
	}
}
