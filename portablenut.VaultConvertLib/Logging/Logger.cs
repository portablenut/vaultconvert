﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace portablenut.VaultConvertLib.Logging
{
	public class Logger : System.MarshalByRefObject
	{
		private DateTime traceTimeStart;
		private string traceTimeTag = string.Empty;

		private object overriddenSender;
		public Logger(object overriddenSender) : this()
		{
			this.overriddenSender = overriddenSender;
		}

		public Logger()
		{
			Filter = LogType.All;
		}

		internal void BeginTraceTime(string tag)
		{
			traceTimeStart = DateTime.Now;
			traceTimeTag = tag;
		}

		internal void EndTraceTime()
		{
			if (!string.IsNullOrWhiteSpace(traceTimeTag) && traceTimeStart != DateTime.MinValue) 
				Trace("{0} (completed in {1})", traceTimeTag, DateTime.Now - traceTimeStart);

			traceTimeStart = DateTime.MinValue;
			traceTimeTag = string.Empty;
		}

		internal void Jitter(string format, params object[] args)
		{
			DoLog(LogType.Jitter, string.Format(format, args));
		}

		internal void Trace(string format, params object[] args)
		{
			DoLog(LogType.Trace, string.Format(format, args));
		}

		internal void Warn(string format, params object[] args)
		{
			DoLog(LogType.Warn, string.Format(format, args));
		}

		internal void Err(Exception exception, string format, params object[] args)
		{
			string evaluatedMessage = string.Format(format, args);
			string fullMessage = string.Format("{0}: \r\n{1}\r\n\r\n{2}",
				evaluatedMessage, exception.Message, exception.StackTrace);

			DoLog(LogType.Error, fullMessage);
		}

		private void DoLog(LogType logType, string message)
		{
			if (Filter.HasFlag(logType) && OnLog != null)
				OnLog(overriddenSender ?? this, new LoggerEventArgs(logType, message));
		}

		public event EventHandler<LoggerEventArgs> OnLog;
		public LogType Filter { get; set; }
	}
}
