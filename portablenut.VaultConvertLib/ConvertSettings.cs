﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text;
using portablenut.VaultConvertLib.Extensions;

namespace portablenut.VaultConvertLib
{
	public class ConvertSettings : System.MarshalByRefObject
	{
		public ConvertSettings(string username, string password, string url, string repository, string gitRepositoryPath)
		{
			this.Username = username;
			this.Password = password;
			this.URL = url;
			this.Repository = repository;
			this.GitRepositoryPath = gitRepositoryPath;
		}

		private ConvertSettings() { }

		public string Username { get; private set; }
		public string Password { get; private set; }
		public string URL { get; private set; }
		public string Repository { get; private set; }
		public string GitRepositoryPath {get; private set;}
	}
}
