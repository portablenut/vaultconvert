﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Text;
using VaultLib;

namespace portablenut.VaultConvertLib.VaultLogic
{
	internal class IsolatedVaultLogic : IDisposable
	{
		private AppDomain appDomain;
		private VaultLogic vaultLogic;

		internal IsolatedVaultLogic()
		{
			Type type = typeof(VaultLogic);
			appDomain = AppDomain.CreateDomain("vaultAppDomain" + Guid.NewGuid().ToString("N"));
			ObjectHandle remoteObj = appDomain.CreateInstanceFrom(type.Assembly.CodeBase, type.FullName);

			vaultLogic = (VaultLogic)remoteObj.Unwrap();
		}

		public void Dispose()
		{
			if (appDomain != null)
				AppDomain.Unload(appDomain);
		}

		public Logging.Logger Logger { get { return vaultLogic.Logger; } set { vaultLogic.Logger = value; } }

		internal void Login(ConvertSettings settings)
		{
			vaultLogic.Login(settings);
		}

		internal SerializableTxInfo GetTransactionInfo(VaultLib.VaultTxHistoryItem version, string repository)
		{
			return vaultLogic.GetTransactionInfo(version, repository);
		}

		internal VaultLib.VaultTxHistoryItem[] GetVersionHistory(long afterVersion, string repository)
		{
			return vaultLogic.GetVersionHistory(afterVersion, repository);
		}

		internal void FetchFilesFromVault(int version, ConvertSettings settings)
		{
			vaultLogic.FetchFilesFromVault(version, settings);
		}
	}
}
