﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VaultLib;

namespace portablenut.VaultConvertLib.VaultLogic
{
	/// <summary>
	/// Serializable version of the TxInfo class provided by the VaultClientIntegrationLib
	/// </summary>
	[Serializable]
	public class SerializableTxInfo
	{
		public string changesetComment;
		public VaultTxDetailHistoryItem[] items;
		public int userid;
		public string userlogin;

		public SerializableTxInfo() { }

		public SerializableTxInfo(VaultClientIntegrationLib.TxInfo txInfo)
		{
			this.userid = txInfo.userid;
			this.userlogin = txInfo.userlogin;
			this.changesetComment = txInfo.changesetComment;
			this.items = txInfo.items;
		}
	}
}
