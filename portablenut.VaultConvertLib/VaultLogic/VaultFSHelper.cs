﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using VaultLib;

namespace portablenut.VaultConvertLib.VaultLogic
{
	public class VaultFSHelper
	{
		private string gitRepositoryPath;

		internal portablenut.VaultConvertLib.Logging.Logger Logger { get; set; }

		public VaultFSHelper(string gitRepositoryPath)
		{
			this.gitRepositoryPath = gitRepositoryPath;
		}

		internal void DoDelete(VaultTxDetailHistoryItem item, GitLogic gitLogic)
		{
			var target = ConvertVaultPathToLocal(item.ItemPath1);

			if (FSHelpers.IsFile(target))
			{
				gitLogic.DeleteFile(target);
				Logger.Jitter("delete {0}", target);
			}
			else if (FSHelpers.IsDirectory(target))
			{
				var deletes = 0;
				foreach (var f in Directory.GetFiles(target, "*.*", SearchOption.AllDirectories))
				{
					gitLogic.DeleteFile(f);
					deletes++;
				}

				Directory.Delete(target, true);
				Logger.Jitter("deletes: {0}", deletes);
			}
			else
				Logger.Warn("failed to delete {0}, doesn't exist ", target);
		}

		internal void DoRename(VaultTxDetailHistoryItem item, GitLogic gitLogic)
		{
			var source = ConvertVaultPathToLocal(item.ItemPath1);

			if (FSHelpers.IsFile(source))
			{
				var target = Path.Combine(Path.GetDirectoryName(source), item.ItemPath2);

				Logger.Jitter("rename {0} to {1}", source, target);
				gitLogic.MoveFile(source, target);
			}
			else if (FSHelpers.IsDirectory(source))
			{
				int renames = 0;
				var targetDir = Path.Combine(Path.GetDirectoryName(source), item.ItemPath2);
				Logger.Jitter("rename dir {0} to {1}", source, targetDir);
				foreach (var file in Directory.GetFiles(source, "*.*", SearchOption.AllDirectories))
				{
					renames++;
					var targetFile = targetDir + file.Substring(source.Length);
					Logger.Jitter("rename {0} to {1}", file, targetFile);
					Directory.CreateDirectory(Path.GetDirectoryName(targetFile));
					gitLogic.MoveFile(file, targetFile);
				}

				DeleteFileOrFolder(source);
				Logger.Jitter("renames {0}", renames);
			}
			else
				Logger.Warn("failed to rename {0}, doesn't exist", source);
		}

		internal void DoMove(VaultTxDetailHistoryItem item, GitLogic gitLogic)
		{

			var source = ConvertVaultPathToLocal(item.ItemPath1);
			var target = ConvertVaultPathToLocal(item.ItemPath2);

			//if repo root is set to $/subfolder and the commit moved file from $/otherfolder to $/subfolder,
			//or other way around, it means target or source will not be available and thus we must escape
			if (string.IsNullOrWhiteSpace(target) || string.IsNullOrWhiteSpace(source))
			{
				Logger.Warn("empty! {0} - {1}", source, target);
				return;
			}

			if (FSHelpers.IsFile(source))
			{
				//this can happen...
				if (File.Exists(target))
				{
					Logger.Jitter("deleting move target {0}", target);
					File.Delete(target);
				}

				gitLogic.MoveFile(source, target);
				Logger.Jitter("Moving {0} to {1}", source, target);
			}
			else if (FSHelpers.IsDirectory(source))
			{
				int moves = 0;
				foreach (var f in Directory.GetFiles(source, "*.*", SearchOption.AllDirectories))
				{
					moves++;
					var p = target + f.Substring(source.Length);
					Directory.CreateDirectory(Path.GetDirectoryName(p));
					Logger.Jitter("move {0} to {1}", f, p);
					gitLogic.MoveFile(f, p);
				}

				DeleteFileOrFolder(source);
				Logger.Jitter("moves {0}", moves);
			}
			else
				Logger.Warn("failed to move {0}", source);
		}

		protected string ConvertVaultPathToLocal(string vaultPath)
		{
			string localRoot = gitRepositoryPath;
			string repoRoot = VaultLogic.VAULT_REPO_ROOT;

			if (vaultPath.StartsWith("$/"))
			{
				if (vaultPath.IndexOf(repoRoot, StringComparison.CurrentCultureIgnoreCase) != 0)
				{
					Logger.Warn("{0} doesn't start with {1}", vaultPath, repoRoot);
					return "";
				}
				else
					return Path.Combine(localRoot, vaultPath.Substring(repoRoot.Length).TrimStart('/'));
			}
			else if (vaultPath == "$")
				return localRoot;
			else
				return Path.Combine(localRoot, vaultPath);
		}

		private void DeleteFileOrFolder(string file)
		{
			var target = ConvertVaultPathToLocal(file);
			var attr = File.GetAttributes(target);

			if (FSHelpers.IsDirectory(target))
			{
				Logger.Jitter("Deleting folder {0} - ({1})", target, file);
				Directory.Delete(target, true);
			}
			else if(FSHelpers.IsFile(target))
			{
				Logger.Jitter("Deleting file {0} - ({1})", target, file);
				File.Delete(target);
			}
		}
	}
}
