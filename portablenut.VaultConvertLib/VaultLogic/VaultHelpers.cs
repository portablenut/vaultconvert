﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using portablenut.VaultConvertLib.Extensions;
using VaultClientIntegrationLib;
using VaultLib;

namespace portablenut.VaultConvertLib.VaultLogic
{
	public static class VaultHelpers
	{
		private static int maxCommitMsgLen = 200;

		/// <summary>
		/// Produces the git commit message
		/// </summary>
		/// <returns></returns>
		public static string GetChangeSetComment(VaultTxHistoryItem version, SerializableTxInfo txInfo)
		{
			//This messy logic will use the changeset comment from vault if any,
			//otherwise it will build one by concatenating the action and file name or
			//the files involved. It will fallback on using a simple "(v. xxx)" if everything else fails
			var comment = version.Comment.NullIfEmpty() ?? txInfo.changesetComment.NullIfEmpty();
			if (String.IsNullOrWhiteSpace(comment))
			{
				StringBuilder sb = new StringBuilder();

				//group commit actions by type and sort them according to a predefined display order
				var actionGroups = txInfo.items.GroupBy(g => g.RequestType).OrderBy(g => GetDisplayOrder(g.Key));

				foreach (var actionGroup in actionGroups)
				{
					if (GetDisplayOrder(actionGroup.Key) != -1 && sb.Length <= maxCommitMsgLen)
					{
						//produce message in format of "CheckIn: folder/some.js, folder/some.html"
						sb.AppendFormat("{0}: ", VaultRequestType.GetRequestTypeName(actionGroup.Key));
						var files = actionGroup.Select(f => f.ItemPath1.NullIfEmpty() ?? f.ItemPath2.NullIfEmpty()).ToArray();
						sb.AppendFormat("{0}\t", String.Join(",", files));
					}
				}

				if (sb.Length == 0)
					comment = string.Format("(v. {0})", version.Version);
				else
					comment = sb.ToString().Truncate(maxCommitMsgLen);
			}

			return comment;
		}

		internal static DateTime ToDateTime(VaultDateTime dt)
		{
			return new DateTime(dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, dt.Second);
		}

		private static int GetDisplayOrder(int requestType)
		{
			switch (requestType)
			{
				case 3:         /* add file */
					return 1;
				case 1:         /* check in */
					return 2;
				case 5:         /* add folder */
					return 3;
				case 15:        /* rename / move */
				case 12:
					return 4;
				case 9:         /* delete */
					return 5;
				default:
					return -1;
			}
		}

	}
}
