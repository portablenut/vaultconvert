﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VaultClientIntegrationLib;
using VaultClientOperationsLib;
using VaultLib;

namespace portablenut.VaultConvertLib.VaultLogic
{
	internal class VaultLogic : System.MarshalByRefObject
	{
		public const string VAULT_REPO_ROOT = "$/";
		private GetOptions getOptions;

		internal portablenut.VaultConvertLib.Logging.Logger Logger { get; set; }

		internal void Login(ConvertSettings settings)
		{
			Logger.Trace("Logging into vault");

			ServerOperations.Logout();
			LoginOptions loginOpt = ServerOperations.client.LoginOptions;

			loginOpt.User = settings.Username;
			loginOpt.Password = settings.Password;
			loginOpt.URL = settings.URL;
			loginOpt.Repository = settings.Repository;

			ServerOperations.Login();
			ServerOperations.client.MakeBackups = false;
			ServerOperations.client.AutoCommit = false;
			ServerOperations.client.Verbose = false;

			Logger.Trace("Logged in!");
		}

		internal VaultTxHistoryItem[] GetVersionHistory(long afterVersion, string repository)
		{
			Logger.BeginTraceTime("GetVersionHistory");
			Logger.Trace("Retrieving history after version {0}", afterVersion);

			ServerOperations.SetRepository(repository);

			var versions = ServerOperations.ProcessCommandVersionHistory(
				VAULT_REPO_ROOT,
				afterVersion + 1,
				VaultDateTime.Parse("1900-01-01"),
				VaultDateTime.Parse("2020-12-31"),
				0).Reverse().ToArray();

			Logger.Trace("Retrieved {0} history items", versions.Length);
			Logger.EndTraceTime();

			return versions;
		}

		internal SerializableTxInfo GetTransactionInfo(VaultTxHistoryItem version, string repository)
		{
			Logger.BeginTraceTime("GetTransactionInfo");
			TxInfo txInfo = ServerOperations.ProcessCommandTxDetail(version.TxID);
			Logger.EndTraceTime();

			return new SerializableTxInfo(txInfo);
		}

		internal void SetupGetOptions(ConvertSettings settings)
		{
			Logger.BeginTraceTime("SetupGetOptions");

			ServerOperations.SetWorkingFolder(VAULT_REPO_ROOT, settings.GitRepositoryPath, true, true);
			getOptions = new GetOptions()
			{
				MakeWritable = MakeWritableType.MakeAllFilesWritable,
				Merge = MergeType.OverwriteWorkingCopy,
				OverrideEOL = VaultEOL.None,
				PerformDeletions = PerformDeletionsType.RemoveWorkingCopy,
				Recursive = true,
				SetFileTime = SetFileTimeType.CheckIn
			};

			Logger.EndTraceTime();
		}

		internal void FetchFilesFromVault(int version, ConvertSettings settings)
		{
			if (getOptions == null)
				SetupGetOptions(settings);

			Logger.BeginTraceTime("FetchFilesFromVault");

			while (true)
			{
				try
				{
					Logger.Trace("Attempting to download files for version {0}", version);
					GetOperations.ProcessCommandGetVersion(VAULT_REPO_ROOT, version, getOptions);
					break;
				}
				catch (GetLatestVersionFailedException)
				{
					Logger.Warn("Failed to download files for version {0}. Will retry", version);
				}
				catch (Exception ex)
				{
					Logger.Err(ex, "Exception thrown when downloading version {0}", version);
					throw;
				}
			}

			Logger.EndTraceTime();
		}
	}
}
