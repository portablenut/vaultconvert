﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace portablenut.VaultConvertLib
{
	public class ConvertProgressEventArgs : EventArgs
	{
		public ConvertProgressEventArgs(long version, long maxVersion, TimeSpan avgIterationTime)
		{
			this.Version = version;
			this.MaxVersion = maxVersion;
			this.AvgIterationTime = avgIterationTime;
		}

		public ConvertProgressEventArgs(string errorMessage)
		{
			this.ErrorMessage = errorMessage;
			this.IsErroneous = true;
		}

		/// <summary>
		/// The vault history version currently being processed
		/// </summary>
		public long Version { get; private set; }

		/// <summary>
		/// Highest vault history version currently known
		/// </summary>
		public long MaxVersion { get; private set; }

		/// <summary>
		/// Average time taking to perform a full iteration, ie fetching a version, staging and committing
		/// </summary>
		public TimeSpan AvgIterationTime { get; private set; }

		/// <summary>
		/// Indicates whether this instance carries an error message, instead of being a simple progress event arg
		/// </summary>
		public bool IsErroneous { get; private set; }

		/// <summary>
		/// Optional error message to send to event subsciber
		/// </summary>
		public string ErrorMessage { get; private set; }
	}
}
