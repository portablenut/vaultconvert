﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace portablenut.VaultConvertLib
{
	public static class FSHelpers
	{
		public static bool IsDirectory(string path)
		{
			try
			{
				var attr = File.GetAttributes(path);
				return ((attr & FileAttributes.Directory) == FileAttributes.Directory);
			}
			catch { return false; }
		}

		public static bool IsFile(string path)
		{
			try
			{
				var attr = File.GetAttributes(path);
				return !((attr & FileAttributes.Directory) == FileAttributes.Directory);
			}
			catch { return false; }
		}
	}
}
