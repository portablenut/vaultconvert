﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using portablenut.VaultConvertLib.Extensions;
using portablenut.VaultConvertLib.Logging;
using portablenut.VaultConvertLib.VaultLogic;
using VaultClientIntegrationLib;
using VaultLib;

namespace portablenut.VaultConvertLib
{
	public class VaultConverter
	{
		private const int NUM_TIME_SAMPLES = 5;

		private GitLogic gitLogic;
		private IsolatedVaultLogic vaultLogic;
		private VaultFSHelper fsHelper;

		private long maxVersion;
		private double[] timeSamples = new double[NUM_TIME_SAMPLES];
		private int sampleIndex = 0;

		public ConvertSettings Settings { get; private set; }
		public long LastCommittedVersion { get; private set; }
		public Logger Logger { get; private set; }

		public event EventHandler<ConvertProgressEventArgs> OnProgress;

		public VaultConverter(ConvertSettings settings)
		{
			this.Settings = settings;
			Logger = new Logger(this);
			Logger.Trace("VaultConverter initialized");
		}

		private void ReportProgress(TimeSpan lastIterationLength)
		{
			double lastIterationMS = lastIterationLength.TotalMilliseconds;
			timeSamples[sampleIndex++ % NUM_TIME_SAMPLES] = lastIterationMS;
			double avg = timeSamples.Average(ts => ts == 0 ? lastIterationMS : ts);
			TimeSpan avgTime = TimeSpan.FromMilliseconds(avg);

			if (OnProgress != null)
				OnProgress(this, new ConvertProgressEventArgs(LastCommittedVersion, maxVersion, avgTime));
		}

		private void ReportProgressError(string errorMessage)
		{
			if (OnProgress != null)
				OnProgress(this, new ConvertProgressEventArgs("Error: " + errorMessage));
		}

		public void Run()
		{
			try
			{
				RunInternal();
			}
			catch (Exception ex)
			{
				Logger.Err(ex, "An error occured");
				ReportProgressError(ex.Message);
			}
		}

		private void RunInternal()
		{
			ReportProgress(new TimeSpan());

			fsHelper = new VaultFSHelper(Settings.GitRepositoryPath);
			fsHelper.Logger = Logger;

			InitializeGitRepository();

			LoginVaultUser();

			LoadState();

			ProcessCommits();
		}

		private void InitializeGitRepository()
		{
			gitLogic = new GitLogic(Settings.GitRepositoryPath);
			gitLogic.Logger = Logger;
			gitLogic.Init();

			gitLogic.CheckDirtyState();
		}

		private void LoginVaultUser()
		{
			vaultLogic = new IsolatedVaultLogic();
			vaultLogic.Logger = Logger;
			vaultLogic.Login(Settings);
		}

		private void LoadState()
		{
			if (gitLogic == null)
				throw new InvalidOperationException("GitLogic class must be initialized before attempting to read state");

			LastCommittedVersion = gitLogic.RetrieveLastCommittedVaultVersion();
		}

		private void ProcessCommits()
		{
			foreach (var version in GetVersionHistory())
			{
				DateTime iterationStart = DateTime.Now;

				gitLogic.CheckDirtyState();

				SerializableTxInfo txInfo = GetTransactionInfo(version);

				PerformLocalFixes(txInfo);

				DownloadFilesForVersion(txInfo, version);

				StageAndCommitChanges(txInfo, version);

				LastCommittedVersion = version.Version;

				ReportProgress(DateTime.Now - iterationStart);
			}
		}

		private VaultTxHistoryItem[] GetVersionHistory()
		{
			VaultTxHistoryItem[] versions = vaultLogic.GetVersionHistory(LastCommittedVersion, Settings.Repository);

			if (versions.Length > 0)
				maxVersion = versions.Last().Version;

			ReportProgress(new TimeSpan());

			return versions;
		}

		private SerializableTxInfo GetTransactionInfo(VaultTxHistoryItem version)
		{
			return vaultLogic.GetTransactionInfo(version, Settings.Repository);
		}

		private void PerformLocalFixes(SerializableTxInfo txInfo)
		{
			Logger.BeginTraceTime("PerformLocalFixes");

			foreach (var tx in txInfo.items.Where(ti => ti.RequestType == VaultRequestType.Delete))
				fsHelper.DoDelete(tx, gitLogic);

			foreach (var tx in txInfo.items.Where(ti => ti.RequestType == VaultRequestType.Move))
				fsHelper.DoMove(tx, gitLogic);

			foreach (var tx in txInfo.items.Where(ti => ti.RequestType == VaultRequestType.Rename))
				fsHelper.DoRename(tx, gitLogic);
				
			Logger.EndTraceTime();
		}

		private void DownloadFilesForVersion(SerializableTxInfo txInfo, VaultTxHistoryItem version)
		{
			vaultLogic.FetchFilesFromVault((int)version.Version, Settings);
		}

		private void StageAndCommitChanges(SerializableTxInfo txInfo, VaultTxHistoryItem version)
		{
			Logger.BeginTraceTime("StageAndCommitChanges");

			gitLogic.StageChanges();

			string changesetComment = VaultHelpers.GetChangeSetComment(version, txInfo);
			string username = txInfo.userlogin.NullIfEmpty() ?? version.UserName;
			DateTime commitDate = VaultHelpers.ToDateTime(version.TxDate);

			gitLogic.CommitChanges(changesetComment, username, commitDate, version.Version);

			Logger.EndTraceTime();
		}
	}
}
