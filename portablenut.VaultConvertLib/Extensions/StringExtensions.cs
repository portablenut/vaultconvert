﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace portablenut.VaultConvertLib.Extensions
{
	public static class StringExtensions
	{
		public static string NullIfEmpty(this string str)
		{
			return string.IsNullOrEmpty(str) ? null : str;
		}

		public static string Truncate(this string str, int length)
		{
			return str.Length <= length ? str : str.Substring(0, length - 3) + "...";
		}
	}
}
