﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace portablenut.VaultConvertRunner.Config
{
	public class RepositoryElement : ConfigurationElement
	{
		[ConfigurationProperty("name", DefaultValue = "", IsKey = false, IsRequired = true)]
		public string Name
		{
			get { return ((string)(base["name"])); }
		}

		[ConfigurationProperty("gitRepositoryPath", IsKey = true, IsRequired = true)]
		public string GitRepositoryPath
		{
			get { return ((string)(base["gitRepositoryPath"])); }
		}
	}
}
