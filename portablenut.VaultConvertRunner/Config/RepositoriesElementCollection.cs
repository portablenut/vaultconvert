﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace portablenut.VaultConvertRunner.Config
{
	[ConfigurationCollection(typeof(RepositoryElement), AddItemName = "repository")]
	public class RepositoriesElementCollection : ConfigurationElementCollection
	{
		protected override ConfigurationElement CreateNewElement()
		{
			return new RepositoryElement();
		}

		protected override object GetElementKey(ConfigurationElement element)
		{
			return ((RepositoryElement)(element)).GitRepositoryPath;
		}

		public RepositoryElement this[int idx]
		{
			get
			{
				return (RepositoryElement)BaseGet(idx);
			}
		}
	}
}
