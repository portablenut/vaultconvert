﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using portablenut.VaultConvertRunner.Config;

namespace portablenut.VaultConvertRunner.Config
{
	public class VaultConvertLibConfigSection : ConfigurationSection
	{
		[ConfigurationProperty("repositories", IsRequired = true)]
		public RepositoriesElementCollection Repositories
		{
			get { return (RepositoriesElementCollection)base["repositories"]; }
		}

		[ConfigurationProperty("settings", IsRequired=true)]
		public SettingsElement Settings
		{
			get { return (SettingsElement)base["settings"]; }
		}

		public static VaultConvertLibConfigSection GetConfig()
		{
			return (VaultConvertLibConfigSection)ConfigurationManager.GetSection("vaultConvertLib");
		}
	}
}
