﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace portablenut.VaultConvertRunner.Config
{
	public class SettingsElement : ConfigurationElement
	{
		[ConfigurationProperty("vaultUser", IsRequired = false)]
		public string VaultUser
		{
			get { return (String)base["vaultUser"]; }
		}

		[ConfigurationProperty("vaultPassword", IsRequired = true)]
		public string VaultPassword
		{
			get { return (String)base["vaultPassword"]; }
		}

		[ConfigurationProperty("vaultUrl", IsRequired = true)]
		public string VaultURL
		{
			get { return (String)base["vaultUrl"]; }
		}

		[ConfigurationProperty("logFolder", IsRequired= true)]
		public string LogFolder
		{
			get { return (String)base["logFolder"]; }
		}
	}
}
