﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using portablenut.VaultConvertLib;
using portablenut.VaultConvertLib.Extensions;
using portablenut.VaultConvertRunner.Config;

namespace portablenut.VaultConvertRunner
{
	class Program
	{
		private static object obj = new object();
		private static string consoleHeaderFormat = "{0, -30} | {1, -8} | {2, -12} | {3, -16}";

		static void OnProgress(VaultConverter sender, ConvertProgressEventArgs e, int displayIndex)
		{
			lock (obj)
			{
				Console.SetCursorPosition(0, displayIndex + 2);
				if (e.IsErroneous)
				{
					Console.WriteLine("{0, -30} | {1, -45}", sender.Settings.Repository, e.ErrorMessage.Remove(44));
				}
				else
				{
					Console.WriteLine(consoleHeaderFormat, sender.Settings.Repository, e.Version, e.MaxVersion, e.AvgIterationTime);
				}
			}
		}

		static void PrintProgressHeader()
		{
			lock (obj)
			{
				Console.SetCursorPosition(0, 0);
				Console.Write(consoleHeaderFormat, "Repository", "Version", "Max Version", "Avg iter.time");

				Console.SetCursorPosition(0, 1);
				Console.Write("".PadRight(Console.WindowWidth, '-'));
			}
		}

		static void OnLog(VaultConverter sender, VaultConvertLib.Logging.LoggerEventArgs e, string logFile)
		{
			File.AppendAllText(logFile, string.Format("{0, -20} {1, -10} {2, -35}\r\n", sender.Settings.Repository, e.LogType, e.Message));
		}

		static void Main(string[] args)
		{
			var config = VaultConvertLibConfigSection.GetConfig();
			var settings = config.Settings;
			var threads = new List<Thread>();

			int displayIndex = 0;
			foreach (var repoConfig in config.Repositories.OfType<RepositoryElement>())
			{
				var setting = new ConvertSettings(
					settings.VaultUser,
					settings.VaultPassword,
					settings.VaultURL,
					repoConfig.Name,
					repoConfig.GitRepositoryPath);

				var logFile = Path.Combine(settings.LogFolder, repoConfig.Name + ".log");

				var converter = new VaultConverter(setting);

				int localDispIndex = displayIndex++;
				converter.OnProgress += (s, e) => OnProgress(s as VaultConverter, e, localDispIndex);
				converter.Logger.OnLog += (s, e) => OnLog(s as VaultConverter, e, logFile);
				converter.Logger.Filter = VaultConvertLib.Logging.LogType.AllButJitter;

				threads.Add(new Thread(converter.Run));
			}

			PrintProgressHeader();
			foreach (var thread in threads)
				thread.Start();

			Console.ReadLine();
		}
	}
}
