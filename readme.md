![vaultConvert](https://bitbucket.org/portablenut/vaultconvert/raw/master/screenshot.png)

#### What's this then?
It's a messy piece of code that converts one or more SourceGear Vault repositories into git repos. Only tested on version 4.1.4 of Vault which is why it contains lots of logic to perform local deletions, moves and renames of files in the working folder, something this version of vault doesn't handle. You should see it as a proof of concept rather than anything else.

#### How about branches?
This tool downloads the repositories in full with no support for branches, for a good reason. I reckon the task of actually download a repository is the most time consuming one and something you'll only want to do once. Splitting the repository into branches should be done as a post-conversion step once the repository is fully downloaded since it will be faster to just re-run that part if the logic is faulty. Also because the way branching is implemented in vault, you'd probably like to customize the behavior anyway to fit your folder structure.

#### Metadata?
Username and commit date will be transferred from vault along with changeset comment if available. If not available, a commit message will be created from the string representation of the file actions involved.

#### Anything else?
If the tool is killed and restarted, it will pick up from where it left and resume the conversion since each git commit is associated with the corresponding vault version by a note.

#### Prerequisites?
You'll need the [Vault Client Libraries](http://download-us.sourcegear.com/Vault/4.1.4.18402/VaultClientAPI_4_1_4_18402.zip) and [LibGit2Sharp](https://github.com/libgit2/libgit2sharp)

#### Performance?
It performs like a dog, but that's mainly due to the slowness of vault. I recommend you keep both the target git repo(s) and the vault cache folder on a ram drive as it will speed up the process considerably. You can create a symbolic link for the SourceGear folder on the ramdrive like this `mklink /d %userprofile%\appdata\local\sourcegear z:\sourcegear`. For a 26,000 commit repository, a single version (download, perform local fixes, stage and commit) starts at roughly 1 sec and slowly creeps up to somewhere around 40-60 sec on a i7 with 16 GB ram drive. Regular GC:ing, backups and optionally a reset of local vault cache are recommended.

